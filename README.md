# Loveletter

Yet another KDE email app. Just for learning purpose. Not ready for daily use.

## Features (plan)

1. Basic email reading and writing.
2. Simple contacts management (local, ownCloud, Nextcloud, Google Contacts).
3. Markdown editor.
4. GPG encryption and signature.

## Technology

Loveletter is not going to reinvent everything. We try use the best existing
solutions, which is stable, of good performance, and well documented.

1. We decided to use Qt Widgets instead of Qt Quick. It will save a lot of
   memory and can run on low-spec devices. When running in background, it won't
   slower the system.
2. We decided to use vmime as IMAP and SMTP back-end. It is written in C++, well
   documented and available in most Linux distributions.

## Build

```
mkdir build
cd build
cmake ../
make

./loveletter
```

## Copyright

2018 Guo Yunhe

## License

GNU General Public License version 3 or later.
