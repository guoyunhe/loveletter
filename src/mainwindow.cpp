// Qt5
#include <QApplication>
#include <QAction>
// KF5
#include <KTextEdit>
#include <KLocalizedString>
#include <KActionCollection>
#include <KStandardAction>
#include <KConfigDialog>

#include "settings.h"

#include "ui_generalopts.h"

#include "mainwindow.h"

class GeneralOptsConfig : public QWidget
{
public:
    GeneralOptsConfig(QWidget *parent)
    : QWidget(parent)
    {
        ui.setupUi(this);
    }

private:
    Ui::GeneralOptsConfig ui;
};

MainWindow::MainWindow(QWidget *parent) : KXmlGuiWindow(parent)
{
    textArea = new KTextEdit();
    setCentralWidget(textArea);
    setupActions();
    setupGUI();
}


void MainWindow::setupActions()
{
    KStandardAction::preferences(this, &MainWindow::configureSettings, actionCollection());
}

void MainWindow::configureSettings()
{
    if ( KConfigDialog::showDialog( QStringLiteral(  "settings" ) ) )
        return;
    KConfigDialog *dialog = new KConfigDialog( this, QStringLiteral( "settings" ), Settings::self() );
    dialog->addPage( new GeneralOptsConfig( dialog ), i18n("General"), QStringLiteral( "games-config-options" ));
    //dialog->addPage( new KgThemeSelector( m_scene->renderer().themeProvider() ), i18n( "Theme" ), QStringLiteral( "games-config-theme" ));
    //dialog->addPage( new CustomGameConfig( dialog ), i18n("Custom Game"), QStringLiteral( "games-config-custom" ));
    //connect( m_scene->renderer().themeProvider(), &KgThemeProvider::currentThemeChanged, this, &KMinesMainWindow::loadSettings );
    connect(dialog, &KConfigDialog::settingsChanged, this, &MainWindow::loadSettings);

    dialog->show();
}

void MainWindow::loadSettings()
{
    //m_view->resetCachedContent();
    // trigger complete redraw
    //m_scene->resizeScene( (int)m_scene->sceneRect().width(),
    //                      (int)m_scene->sceneRect().height() );
}
