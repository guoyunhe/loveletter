#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <KXmlGuiWindow>

class KTextEdit;

class MainWindow : public KXmlGuiWindow
{
public:
    explicit MainWindow(QWidget *parent = nullptr);
    void configureSettings();
    void loadSettings();

private:
    KTextEdit* textArea;
    void setupActions();
};

#endif
