#include <cstdlib>

#include <QApplication>
#include <QCommandLineParser>

#include <KAboutData>
#include <KLocalizedString>

#include <vmime/vmime.hpp>
#include <vmime/utility/url.hpp>
#include <vmime/utility/urlUtils.hpp>
#include <vmime/net/session.hpp>
#include <vmime/net/store.hpp>

#include "mainwindow.h"

int main (int argc, char *argv[])
{
    vmime::utility::url url("imap", "imap.gmail.com", 993, "", "guoyunhebrave@gmail.com", "waffizndyjfabsbn");
    vmime::shared_ptr<vmime::net::session> sess = vmime::net::session::create();
    vmime::shared_ptr<vmime::net::store> store = sess->getStore(url);
    // store->connect();

    QApplication app(argc, argv);
    KLocalizedString::setApplicationDomain("loveletter");

    KAboutData aboutData(
        // The program name used internally. (componentName)
        QStringLiteral("loveletter"),
        // A displayable program name string. (displayName)
        i18n("Loveletter"),
        // The program version string. (version)
        QStringLiteral("1.0"),
        // Short description of what the app does. (shortDescription)
        i18n("Yet another email client for KDE"),
        // The license this code is released under
        KAboutLicense::GPL_V3,
        // Copyright Statement (copyrightStatement = QString())
        i18n("(c) 2018"),
        // Optional text shown in the About box.
        // Can contain any information desired. (otherText)
        i18n("Some text..."),
        // The program homepage string. (homePageAddress = QString())
        QStringLiteral("https://gitlab.com/guoyunhe/loveletter"),
        // The bug report email address
        // (bugsEmailAddress = QLatin1String("submit@bugs.kde.org")
        QStringLiteral("yunhe.guo@protonmail.com"));
    aboutData.addAuthor(i18n("Guo Yunhe"), i18n("Author"), QStringLiteral("yunhe.guo@protonmail.com"),
                        QStringLiteral("https://guoyunhe.me/"), QStringLiteral("guoyunhe"));
    KAboutData::setApplicationData(aboutData);

    QCommandLineParser parser;
    aboutData.setupCommandLine(&parser);
    parser.process(app);
    aboutData.processCommandLine(&parser);

    MainWindow* window = new MainWindow();
    window->show();

    return app.exec();
}
